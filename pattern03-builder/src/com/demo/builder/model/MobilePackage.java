/**
 * 
 */
package com.demo.builder.model;

/**
 * 手机套餐
 * 
 * @author karl
 *
 */
public class MobilePackage {
	
	// 话费
	private float money;
	// 短信
	private int shortInfo;
	// 彩铃
	private String music;
	/**
	 * @return the money
	 */
	public float getMoney() {
		return money;
	}
	/**
	 * @param money the money to set
	 */
	public void setMoney(float money) {
		this.money = money;
	}
	/**
	 * @return the shortInfo
	 */
	public int getShortInfo() {
		return shortInfo;
	}
	/**
	 * @param shortInfo the shortInfo to set
	 */
	public void setShortInfo(int shortInfo) {
		this.shortInfo = shortInfo;
	}
	/**
	 * @return the music
	 */
	public String getMusic() {
		return music;
	}
	/**
	 * @param music the music to set
	 */
	public void setMusic(String music) {
		this.music = music;
	}
	
	

}
