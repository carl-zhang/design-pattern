package com.demo.builder.director;

import com.demo.builder.itf.IMobileBuilder;
import com.demo.builder.model.MobilePackage;

/**
 * 手机套餐指导者
 * 
 * @author karl
 *
 */
public class MobileDirector {
	
	public MobilePackage createMobilePackage(IMobileBuilder mobileBuilder) {
		if(mobileBuilder != null) {
			// 构建话费
			mobileBuilder.buildMoney();
			// 构建短信
			mobileBuilder.buildShortInfo();
			// 构建彩铃
			mobileBuilder.buildMusic();
			
			// 返回手机套餐
			return mobileBuilder.getMobilePackage();
		}
		
		return null;
	}

}
