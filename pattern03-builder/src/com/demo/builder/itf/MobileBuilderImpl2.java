/**
 * 
 */
package com.demo.builder.itf;

import com.demo.builder.base.AbstractBasePackage;
import com.demo.builder.model.MobilePackage;

/**
 * 套餐1
 * 
 * @author karl
 *
 */
public class MobileBuilderImpl2 extends AbstractBasePackage implements IMobileBuilder {

	@Override
	public void buildMoney() {
		// TODO Auto-generated method stub
		mobilePackage.setMoney(30.0f);
	}

	@Override
	public void buildShortInfo() {
		// TODO Auto-generated method stub
		mobilePackage.setShortInfo(600);
	}

	@Override
	public void buildMusic() {
		// TODO Auto-generated method stub
		mobilePackage.setMusic("大海");
	}

	@Override
	public MobilePackage getMobilePackage() {
		// TODO Auto-generated method stub
		return mobilePackage;
	}

}
