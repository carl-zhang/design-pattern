/**
 * 
 */
package com.demo.builder.itf;

import com.demo.builder.model.MobilePackage;

/**
 * 手机套餐Builder
 * 
 * @author karl
 *
 */
public interface IMobileBuilder {
	
	// 建造手机套餐的话费
	public void buildMoney();
	// 建造手机套餐的短信
	void buildShortInfo();
	// 建造手机套餐的彩铃
	void buildMusic();
	
	// 返回建造的手机套餐对象
	MobilePackage getMobilePackage();

}
