package com.demo.factory;

import com.demo.factory.custom.Customer;
import com.demo.factory.itf.ChinaKfcFactory;
import com.demo.factory.itf.IKfcFactory;

public class MainApp {

	public static void main(String[] args) {
		// 定义一个肯德基
		IKfcFactory kfcFactory = new ChinaKfcFactory();
		// 客户进来，准备点餐
		Customer customer = new Customer(kfcFactory);
		float hamburgMoney = customer.orderHamburg(1);
		float chickenWingsMoney = customer.orderChickenWings(4);
		float frenchFriesMoney = customer.orderFrenchFries(1);
		float beverageMoney = customer.orderBeverage(2);
		
		System.out.println("总计："+ (hamburgMoney + chickenWingsMoney 
				+ frenchFriesMoney + beverageMoney));

	}

}
