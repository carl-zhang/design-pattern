/**
 * 
 */
package com.demo.factory.model;

/**
 * 食物基类</br>
 * 该类中含有类别、数量和单价几个属性，还有一个计算总价的方法
 * 
 * @author karl
 *
 */
public abstract class AbstractBaseFood {
	// 类别
	protected String kind;
	// 数量
	protected int num;
	// 价格
	protected float price;
	// 合计
	public float totalPrice() {
		return this.num * this.price;
	}
}
