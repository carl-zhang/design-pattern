/**
 * 
 */
package com.demo.factory.model.kfc;

import com.demo.factory.model.FrenchFries;
import com.demo.factory.model.Hamburg;

/**
 * 薯条实现类
 * 
 * @author karl
 *
 */
public class ChinaFrenchFries extends FrenchFries {

	/**
	 * 
	 */
	public ChinaFrenchFries(int num) {
		this.kind = "普通";
		this.price = 8.0f;
		this.num = num;
	}

}
