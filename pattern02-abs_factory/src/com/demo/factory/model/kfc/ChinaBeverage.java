/**
 * 
 */
package com.demo.factory.model.kfc;

import com.demo.factory.model.Beverage;
import com.demo.factory.model.Hamburg;

/**
 * 饮料实现类
 * 
 * @author karl
 *
 */
public class ChinaBeverage extends Beverage {

	/**
	 * 
	 */
	public ChinaBeverage(int num) {
		this.kind = "可乐";
		this.price = 7.0f;
		this.num = num;
	}

}
