/**
 * 
 */
package com.demo.factory.model;

/**
 * 抽象食物接口
 * 
 * @author karl
 *
 */
public interface IFood {
	/**
	 * 打印输出食物信息
	 */
	void printMessage();
}
