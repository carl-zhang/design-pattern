/**
 * 
 */
package com.demo.factory.model;

/**
 * 鸡翅基类
 * 
 * @author karl
 *
 */
public abstract class ChickenWings extends AbstractBaseFood implements IFood {

	/**
	 * 
	 */
	public ChickenWings() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void printMessage() {
		System.out.println("--"+ this.kind +"风味鸡翅，\t单价："
	+ this.price +",\t数量："
	+ this.num +",\t合计："
	+ this.totalPrice());

	}

}
