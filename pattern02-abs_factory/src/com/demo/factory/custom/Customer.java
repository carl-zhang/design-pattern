/**
 * 
 */
package com.demo.factory.custom;

import com.demo.factory.itf.IKfcFactory;
import com.demo.factory.model.Beverage;
import com.demo.factory.model.ChickenWings;
import com.demo.factory.model.FrenchFries;
import com.demo.factory.model.Hamburg;

/**
 * 
 * 客户类<br/>
 * 类中含有一个抽象工厂IKfcFactory类型的实例变量kfcFactory，<br/>
 * 客户类Customer通过构造方法将肯德基店实例传入，客户需要食物时，就向肯德基店（工厂）请求，<br/>
 * 客户不生产食物（不使用new生成对象）
 * 
 * @author karl
 *
 */
public class Customer {
	
	private IKfcFactory kfcFactory;
	

	/**
	 * 构造方法将抽象工厂作为参数传入
	 */
	public Customer(IKfcFactory kfcFactory) {
		this.kfcFactory = kfcFactory;
	}
	
	/**
	 * 订购麻辣鸡腿汉堡
	 * @param num 订购数量
	 * @return 总价格
	 */
	public float orderHamburg(int num) {
		Hamburg hamburg = kfcFactory.createHamburg(num);
		hamburg.printMessage();
		
		return hamburg.totalPrice();
	}
	/**
	 * 订购鸡翅
	 * @param num 鸡翅数量
	 * @return 总价格
	 */
	public float orderChickenWings(int num) {
		ChickenWings chickenWings = kfcFactory.createChickenWings(num);
		chickenWings.printMessage();
		
		return chickenWings.totalPrice();
	}
	/**
	 * 订购薯条
	 * @param num 薯条数量
	 * @return 总价格
	 */
	public float orderFrenchFries(int num) {
		FrenchFries frenchFries = kfcFactory.createFrenchFries(num);
		frenchFries.printMessage();
		return frenchFries.totalPrice();
	}
	
	/**
	 * 订购可乐
	 * @param num 可乐数量
	 * @return 总价格
	 */
	public float orderBeverage(int num) {
		Beverage beverage = kfcFactory.createBeverage(num);
		beverage.printMessage();
		return beverage.totalPrice();
	}

}
