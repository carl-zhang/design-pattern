/**
 * 
 */
package com.demo.factory.itf;

import com.demo.factory.model.Beverage;
import com.demo.factory.model.ChickenWings;
import com.demo.factory.model.FrenchFries;
import com.demo.factory.model.Hamburg;
import com.demo.factory.model.kfc.ChinaBeverage;
import com.demo.factory.model.kfc.ChinaChickenWings;
import com.demo.factory.model.kfc.ChinaFrenchFries;
import com.demo.factory.model.kfc.ChinaHanburm;

/**
 * 具体工厂
 * 
 * @author karl
 *
 */
public class ChinaKfcFactory implements IKfcFactory {

	/**
	 * 
	 */
	public ChinaKfcFactory() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Hamburg createHamburg(int num) {
		// TODO Auto-generated method stub
		return new ChinaHanburm(num);
	}

	@Override
	public FrenchFries createFrenchFries(int num) {
		// TODO Auto-generated method stub
		return new ChinaFrenchFries(num);
	}

	@Override
	public ChickenWings createChickenWings(int num) {
		// TODO Auto-generated method stub
		return new ChinaChickenWings(num);
	}

	@Override
	public Beverage createBeverage(int num) {
		// TODO Auto-generated method stub
		return new ChinaBeverage(num);
	}

}
