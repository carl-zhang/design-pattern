/**
 * 
 */
package com.demo.subject;

import java.util.Iterator;
import java.util.Vector;

import com.demo.observer.IObserver;

/**
 * 主题实现类（被观察者）
 * 
 * @author karl
 *
 */
public class Subject implements ISubject {
	
	// 温度
	/**
	 * （一）高温黄色预警信息
	 * 
	 * 标准：连续三天日最高气温将在35℃以上。
	 * 
	 * （二）高温橙色预警信号
	 * 
	 * 标准：24小时内最高气温升至37℃以上。
	 * 
	 * （三）高温红色预警信号
	 * 
	 * 标准：24小时内最高气温升至40℃以上。
	 */
	
	private float temperature;
	// 预警级别
	private String warningLevel;
	
	// 保存观察者列表
	private Vector<IObserver> vector;
	
	/**
	 * 构造方法，初始化观察者列表
	 */
	public Subject() {
		vector = new Vector<IObserver>();
	}

	@Override
	public boolean add(IObserver observer) {
		if(observer != null && !vector.contains(observer))
			return vector.add(observer);
		
		return false;
	}

	@Override
	public boolean remove(IObserver observer) {
		// TODO Auto-generated method stub
		return vector.remove(observer);
	}

	@Override
	public void notifyAllObserver() {
		System.out.println("\n-----气象部门发布高温"+ this.warningLevel
				+"警报！-----");
		Iterator<IObserver> iterator = vector.iterator();
		while(iterator.hasNext()) {
			(iterator.next()).update(this);
		}

	}
	
	/**
	 * 根据温度值设置预警级别，然后通知所有观察者
	 * 
	 * 说明：此处电子书上的内容不全，
	 * 并且是从 >= 35 开始，但实际操作中并没有打印预期内容，遂做了一些改动
	 * 
	 */
	private void invoke() {
		
		// 设置预警级别
		if(this.temperature >= 40) {
			warningLevel = "红色";
		} else if(this.temperature >= 37) {
			warningLevel = "橙色";
		} else if(this.temperature >= 35) {
			warningLevel = "黄色";
		}  else {
			warningLevel = "";
		}
		// 通知所有观察者
		if(this.temperature >= 35) {
			this.notifyAllObserver();
		}

	}

	@Override
	public void setTemperature(float temperature) {
		this.temperature = temperature;
		invoke();
	}

	@Override
	public String temperatureReport() {
		// TODO Auto-generated method stub
		return "温度："+ temperature;
	}

}
