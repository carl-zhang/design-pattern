/**
 * 
 */
package com.demo.observer;

import com.demo.subject.ISubject;

/**
 * @author karl
 *
 */
public class GovernmentObserver implements IObserver {

	@Override
	public void update(ISubject subject) {
		System.out.println("政府部门收到高温预警："+ subject.temperatureReport());

	}

}
