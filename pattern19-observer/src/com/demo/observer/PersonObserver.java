/**
 * 
 */
package com.demo.observer;

import com.demo.subject.ISubject;

/**
 * 
 * 
 * @author karl
 *
 */
public class PersonObserver implements IObserver {

	@Override
	public void update(ISubject subject) {
		System.out.println("个人收到高温预警："+ subject.temperatureReport());

	}

}
