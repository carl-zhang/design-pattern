/**
 * 
 */
package com.demo.observer;

import com.demo.subject.ISubject;

/**
 * @author karl
 *
 */
public class CompanyObserver implements IObserver {

	@Override
	public void update(ISubject subject) {
		System.out.println("企事业单位收到预警："+ subject.temperatureReport());

	}

}
