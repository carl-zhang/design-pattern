package com.demo;

import com.demo.factory.itf.ISwordFactory;
import com.demo.factory.model.AbstractSword;
import com.demo.factory.object.Caocao;
import com.demo.factory.object.Caocao2;

public class MainApp {

	public static void main(String[] args) {
		
		/*=====七星宝刀场景=====*/
		// 创建曹操实例对象
		// ISwordFactory swordFactory = new Caocao();
		// 获得七星宝刀
		// AbstractSword sword = swordFactory.createSword();
		// 刺杀董卓
		// System.out.println("曹操使用："+ sword.getName() +"刺杀董卓！");
		
		/*=====八星宝刀场景=====*/
		// 创建曹操实例对象（使用接口类型 ISwordFactory）
		ISwordFactory swordFactory = new Caocao2();
		// 获得宝刀实例——八星宝刀
		AbstractSword sword = swordFactory.createSword();
		// 刺杀董卓
		System.out.println("曹操使用"+ sword.getName() +"刺杀董卓");
		
	}

}
