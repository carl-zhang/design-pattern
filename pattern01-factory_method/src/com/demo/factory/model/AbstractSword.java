/**
 * 
 */
package com.demo.factory.model;

/**
 * 定义抽象宝刀
 * 
 * @author karl
 *
 */
public abstract class AbstractSword {
	
	// 宝刀名称
	private String name;

	/**
	 * 抽象父类的构造方法
	 */
	public AbstractSword() {
		System.out.println("宝刀父类");
	}

	/**
	 * 获得宝刀名称
	 * @return the name
	 */
	public String getName() {
		System.out.println("获取宝刀名称");
		return name;
	}

	/**
	 * 设置宝刀名称
	 * @param name the name to set
	 */
	public void setName(String name) {
		System.out.println("设置宝刀名称");
		this.name = name;
	}
	
	
	

}
