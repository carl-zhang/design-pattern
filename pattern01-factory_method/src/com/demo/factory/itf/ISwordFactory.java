/**
 * 
 */
package com.demo.factory.itf;

import com.demo.factory.model.AbstractSword;

/**
 * 
 * 宝刀工厂
 * 
 * @author karl
 *
 */
public interface ISwordFactory {
	
	/**
	 * 生产各类宝刀
	 * @return  An abstract sword
	 */
	public AbstractSword createSword();
	
}
