/**
 * 
 */
package com.demo.factory.object;

import com.demo.factory.itf.ISwordFactory;
import com.demo.factory.model.AbstractSword;

/**
 * 曹操具体工厂
 * 
 * @author karl
 *
 */
public class Caocao implements ISwordFactory {

	/**
	 * 
	 */
	public Caocao() {
		System.out.println("这是曹操");
	}

	/**
	 * 实现ISwordFactory接口的createSword方法，生产七星宝刀！
	 */
	@Override
	public AbstractSword createSword() {
		System.out.println("曹操生产七星宝刀");
		return new QixingSword();
	}

}
