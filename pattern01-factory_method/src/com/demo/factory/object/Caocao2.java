/**
 * 
 */
package com.demo.factory.object;

import com.demo.factory.itf.ISwordFactory;
import com.demo.factory.model.AbstractSword;

/**
 * 使用八星宝刀的曹操具体工厂
 * 
 * @author karl
 *
 */
public class Caocao2 implements ISwordFactory {

	/**
	 * 
	 */
	public Caocao2() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 生产八星宝刀
	 */
	@Override
	public AbstractSword createSword() {
		// TODO Auto-generated method stub
		return new BaxingSword();
	}

}
