package com.demo.factory.object;

import com.demo.factory.model.AbstractSword;
/**
 * 七星宝刀类
 * 
 * @author karl
 *
 */
public class QixingSword extends AbstractSword {

	/**
	 * 构造方法设置七星宝刀的名称
	 */
	public QixingSword() {
		System.out.println("七星宝刀");
		this.setName("七星宝刀");
	}

}
