/**
 * 
 */
package com.demo.singleton2;

/**
 * 
 * 单例设计模式二
 * 
 * @author karl
 *
 */
public class Singleton {
	
	// 类共享实例对象实例化
	private static Singleton singleton = new Singleton();
	// 私有构造方法
	private Singleton() {
		System.out.println("-- This is Singleton!");
	}
	
	// 获得单例方法
	public static Singleton getInstance() {
		// 直接返回共享对象
		return singleton;
	}

}
