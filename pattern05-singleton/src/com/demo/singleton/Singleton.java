/**
 * 
 */
package com.demo.singleton;

/**
 * 
 * 单例设计模式一<br/>
 * 三个要点：<br/>
 * 1. 一个全局对象实例；<br/>
 * 2. 一个私有构造方法；<br/>
 * 3. 一个获得单例对象的方法。
 * 
 * @author karl
 *
 */
public class Singleton {
	
	// 1. static类型的全局类实例对象
	private static Singleton singleton = null;
	
	// 2. 私有构造方法
	private Singleton() {
		System.out.println("=== This is Singleton! ===");
	}
	
	// 3. 获得单例方法
	public synchronized static Singleton getInstance() {
		// 判断共享对象是否为 null，如果为null 则创建一个对象
		if(null == singleton) {
			singleton = new Singleton();
		}
		
		return singleton;
	}

}
