/**
 * 
 */
package com.demo.singleton;

/**
 * @author karl
 *
 */
public class Client {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// 首先检验是否能用 new 操作符创建Singleton对象实例
		Singleton singleton = Singleton.getInstance();
		// 获得Singleton对象实例
		Singleton singleton2 = Singleton.getInstance();
		// 比较两个对象是否为同一个对象实例
		if(singleton == singleton2) {
			System.out.println("---这是同一个对象");
		} else {
			System.out.println("---这不是同一个对象");
		}
		
	}

}
