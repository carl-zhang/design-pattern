package com.demo;

import com.demo.multipleton.Multipleton;

public class MultipletonClient {

	public static void main(String[] args) {
		// 获得Multipleton对象实例
		Multipleton multipleton = Multipleton.getRandomInstance();
		System.out.println("multipleton:"+ multipleton.getNo());
		// 再次获得对象实例
		Multipleton multipleton2 = Multipleton.getRandomInstance();
		System.out.println("multipleton2:"+ multipleton2.getNo());
		System.out.println("=====================");
		
		// 比较两个对象实例
		if(multipleton == multipleton2) {
			System.out.println("--这是同一个对象！");
		} else {
			System.out.println("--这是不同的对象");
		}

	}

}
