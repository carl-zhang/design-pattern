# design pattern

#### Description
《软件秘笈——设计模式那点事》中模式代码实现

#### Software Architecture
23种设计模型的Java实现，以及简要说明。

#### Installation

#### Instructions

1.  在Eclipse下实现
2.  使用Java语言
3.  实例来自《软件秘笈——设计模型那点事》

#### Contribution

1.  30/04/2020 创建工程并实现了第一个模型：工厂方法模型
2.  03/05/2020 实现抽象工厂模型
3.  04/05/2020 实现建造者模式
4.  04/05/2020 实现单例模式


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
