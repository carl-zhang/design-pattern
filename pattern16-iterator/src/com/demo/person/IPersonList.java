/**
 * 
 */
package com.demo.person;

import java.util.ArrayList;

import com.demo.iterator.Iterator;

/**
 * 人员接口
 * 
 * @author karl
 *
 */
public interface IPersonList {
	
	/**
	 * 获得内部存储人员信息内容
	 * 
	 * @return
	 */
	public ArrayList<IPerson> getPersonList();
	
	/**
	 * 迭代器
	 * 
	 * @return
	 */
	public Iterator iterator();

}
