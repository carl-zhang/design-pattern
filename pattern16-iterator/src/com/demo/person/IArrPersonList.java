/**
 * 
 */
package com.demo.person;

import java.util.ArrayList;

import com.demo.iterator.Iterator;

/**
 * 人员接口
 * 
 * @author karl
 *
 */
public interface IArrPersonList {
	
	/**
	 * 获得内部存储人员信息内容
	 * 
	 * @return
	 */
	public IPerson[] getPersonList();
	
	/**
	 * 迭代器
	 * 
	 * @return
	 */
	public Iterator iterator();

}
