/**
 * 
 */
package com.demo.person;

import java.util.ArrayList;
import java.util.Random;

import com.demo.iterator.Iterator;
import com.demo.iterator.PersonIterator;

/**
 * 人员信息列表
 * 
 * @author karl
 *
 */
public class PersonList implements IPersonList {
	
	
	// 存储用户信息列表
	private ArrayList<IPerson> list = new ArrayList<IPerson>();
	
	// 构造方法初始化人员信息

	public PersonList() {
		Random random = new Random();
		// 创建人员信息
		for(int i = 0; i < 10; i++) {
			IPerson person = new Person("人员"+ i, random.nextInt(30), random.nextInt(2));
			this.list.add(person);
		}
	}

	@Override
	public ArrayList<IPerson> getPersonList() {
		// TODO Auto-generated method stub
		return this.list;
	}

	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return new PersonIterator(this.list);
	}

}
