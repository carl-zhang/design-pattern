/**
 * 
 */
package com.demo.person;

/**
 * 人员信息
 * 
 * @author karl
 *
 */
public interface IPerson {

	/**
	 * 获得人员信息
	 * @return 人员信息
	 */
	public String getPersonInfo();
	
}
