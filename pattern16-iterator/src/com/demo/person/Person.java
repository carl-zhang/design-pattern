/**
 * 
 */
package com.demo.person;

/**
 * 人员信息具体实现类
 * 
 * @author karl
 *
 */
public class Person implements IPerson {
	
	// 姓名
	private String name;
	// 年龄
	private int age;
	// 性别（1：男性 0：女性）
	private int sex;
	
	
	
	/**
	 * 构造方法设置属性内容
	 * 
	 * @param name
	 * @param age
	 * @param sex
	 */
	public Person(String name, int age, int sex) {
		super();
		this.name = name;
		this.age = age;
		this.sex = sex;
	}



	@Override
	public String getPersonInfo() {
		// TODO Auto-generated method stub
		return getInfo();
	}
	
	public String getInfo() {
		String name = "姓名："+ this.name;
		String age = " - 年龄："+ this.age;
		String sex = " - 性别："+ (this.sex == 1 ? "男":(this.sex == 0 ? "女":""));
		
		return name + age + sex;
	}

}
