/**
 * 
 */
package com.demo.person;

import java.util.ArrayList;
import java.util.Random;

import com.demo.iterator.ArrPersonIterator;
import com.demo.iterator.Iterator;

/**
 * 人员信息列表
 * 
 * @author karl
 *
 */
public class ArrPersonList implements IArrPersonList {
	
	
	// 存储用户信息列表
	private IPerson[] list = new IPerson[10];
	
	// 构造方法初始化人员信息

	public ArrPersonList() {
		Random random = new Random();
		// 创建人员信息
		for(int i = 0; i < 10; i++) {
			IPerson person = new Person("人员"+ i, random.nextInt(30), random.nextInt(2));
			this.list[i] = person;
		}
	}


	/**
	 * 该方法继承自IPersonList接口，是不可以这么直接更改返回值的,
	 * 需要重新建立接口类
	 */
	@Override
	 public IPerson[] getPersonList() {
		 
		 return this.list; 
	 }
	 
	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return new ArrPersonIterator(this.list);
	}

}
