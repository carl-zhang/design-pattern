package com.demo.iterator;

import java.util.ArrayList;

import com.demo.person.IPerson;

/**
 * 具体人员迭代器实现
 * 
 * @author karl
 *
 */
public class ArrPersonIterator implements Iterator {
	// 私有属性存储人员列表对象信息
	private final IPerson[] personList;
	// 存储位置信息，初始值为-1
	private int index = -1;
	
	

	/**
	 * 构造方法将人员列表对象传入
	 * 
	 * @param personList
	 */
	public ArrPersonIterator(IPerson[] personList) {
		super();
		this.personList = personList;
	}

	@Override
	public boolean hasNext() {
		boolean flag = this.personList == null
				?false:(index < this.personList.length - 1);
		
		return flag;
	}

	@Override
	public Object next() {
		if(hasNext()) {
			// 获得人员列表对象中的人员信息
			return this.personList[++index];
		}
		
		return null;
	}

	@Override
	public Object remove() {
		if (this.personList != null) {
			IPerson person = this.personList[index];
			this.personList[index] = null;
			
			return person;
		}	
		return null;
	}

}
