package com.demo;

import java.util.ArrayList;

import com.demo.iterator.Iterator;
import com.demo.person.ArrPersonList;
import com.demo.person.IArrPersonList;
import com.demo.person.IPerson;
import com.demo.person.IPersonList;
import com.demo.person.PersonList;

public class Client {

	public static void main(String[] args) {
		// 创建人员列表对象
		IPersonList personList = new PersonList();
		System.out.println("----使用内部对象输出人员信息 start......");
		ArrayList<IPerson> arrayList = personList.getPersonList();
		
		for (int i = 0; i < arrayList.size(); i++) {
			// 输出人员信息
			System.out.println(arrayList.get(i).getPersonInfo());
		}
		System.out.println("----使用内部对象输出人员信息 end......");
		System.out.println("----使用迭代器输出人员信息 start......");
		// 生成迭代器
		Iterator iterator = personList.iterator();
		while (iterator.hasNext()) {
			// 获得人员对象实例
			IPerson person = (IPerson) iterator.next();
			if(person != null) {
				// 输出人员信息
				System.out.println(person.getPersonInfo());
			}
			
			
		}
		System.out.println("----使用迭代器输出人员信息 end......");
		System.out.println("----使用迭代器测试数组类型 start......");
		IArrPersonList arrPersonList = new ArrPersonList();
		Iterator iterator2 = arrPersonList.iterator();
		while (iterator2.hasNext()) {
			// 获得人员对象实例
			IPerson person = (IPerson) iterator2.next();
			if(person != null) {
				// 输出
				System.out.println(person.getPersonInfo());
			}
		}
		System.out.println("----使用迭代器测试数组类型 end......");
	

	}

}
