/**
 * 
 */
package com.demo.power.v12;

/**
 * 12V 电源接口
 * 
 * @author karl
 *
 */
public interface IPower12 {
	// 12V 交流电源打印
	public void output12v();
}
