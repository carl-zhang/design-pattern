/**
 * 
 */
package com.demo.power.v220;

/**
 * 220V 电源接口
 * 
 * @author karl
 *
 */
public interface IPower220 {
	// 220V 交流电源打印
	public void output220v();
}
