/**
 * 
 */
package com.demo.power.v220;

import com.demo.power.AbsBasePower;

/**
 * @author karl
 *
 */
public class Power220 extends AbsBasePower implements IPower220 {
	
	

	/**
	 * 构造方法
	 */
	public Power220() {
		super(220);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void output220v() {
		// TODO Auto-generated method stub
		System.out.println("----这是["
		+ this.getPower() 
		+ this.getUnit() 
		+"]电源！...");
	}

}
