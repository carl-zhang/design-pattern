/**
 * 
 */
package com.demo.power;

/**
 * 电源基类
 * 
 * @author karl
 *
 */
public abstract class AbsBasePower {
	// 电压值
	private float power;
	// 单位
	private String unit = "V";
	/**
	 * @param power
	 */
	public AbsBasePower(float power) {
		super();
		this.power = power;
	}
	/**
	 * @return the power
	 */
	public float getPower() {
		return power;
	}
	/**
	 * @param power the power to set
	 */
	public void setPower(float power) {
		this.power = power;
	}
	/**
	 * @return the unit
	 */
	public String getUnit() {
		return unit;
	}
	/**
	 * @param unit the unit to set
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	
}
